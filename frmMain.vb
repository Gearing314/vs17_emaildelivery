﻿Imports System.ComponentModel
Imports System.Data.SqlClient
Imports System.IO

Public Class frmMain

    Private connectionString As String = "Data Source=DILBERT\SQL2012DEV;Initial Catalog=ContactsOnSQL;User ID =sa;Password=23640%i"
    Private sql As New clsSqlTools(connectionString)
    Private email As New clsSMTP()
    Private companies As New List(Of Integer)
    Private fileReader As StreamReader
    Private emailString As String

    Private Sub btn_start_Click(sender As Object, e As EventArgs) Handles btn_start.Click
        Dim interval As Integer = Convert.ToInt32(txt_interval.Text)    'Puts value in milliseconds
        timer.Interval = interval * 1000 * 60
        Dim confirm As MsgBoxResult
        Dim message As String = "This email will be sent to each company in the database." + vbNewLine + "The spacing between the emails for each company will be " + interval.ToString + " minutes " + vbNewLine + "Are you sure you wish to start?"
        confirm = MsgBox(message, MsgBoxStyle.YesNo, "Confirm") 'Double check

        If confirm = MsgBoxResult.Yes Then
            fileReader = New StreamReader(txt_file.Text)
            emailString = fileReader.ReadToEnd

            getCompanies()  'Gets the company ids (DUMMY STATEMENT ATM)!!!!
            timer.Enabled = True    'Starts emailing process
            currentTimer.Enabled = True
            Enabled = False 'Disables main form
            pb_continous.MarqueeAnimationSpeed = 30
        End If
    End Sub

    Private tickCount As Int32 = 0
    Private Sub timer_Tick(sender As Object, e As EventArgs) Handles timer.Tick
        'Send emails for one company
        lbl_current.Text = "Sending Emails to "
        Dim companyEmails As New List(Of String)
        Dim getCompanies As String = "Select Email from Contact where CompanyID =" + companies(tickCount).ToString + " and Newsletter=1"
        Dim reader As SqlDataReader = sql.query(getCompanies)
        'Gets all email addresses from companies
        Try
            If reader.HasRows() Then
                Do While reader.Read()
                    Try
                        If reader.GetString(0) <> Nothing Then
                            companyEmails.Add(reader.GetString(0))
                        End If
                    Catch ex As Exception
                        Debug.WriteLine("Email not found")
                    End Try
                Loop
            Else
                MsgBox("Doesn't have rows")
            End If
        Catch ex As Exception
            GoTo end_of_sub 'skips the rest of the function
        End Try
        'Closes reader and connection VERY IMPORTANT
end_of_sub:
        sql.closeConnection()

        For Each clientEmail In companyEmails
            email.SendMessage("office@stringerscales.co.uk", clientEmail, "", "", "Newsletter Confirmation", emailString, True, chk_HTML.Checked)
        Next
        pb_overall.Increment(100 / companies.Count)
        tickCount += 1
        If tickCount = companies.Count Then
            timer.Enabled = False
            currentTimer.Enabled = False
            Enabled = True
            lbl_current.Text = ""
            pb_overall.Increment(-100)
            pb_continous.MarqueeAnimationSpeed = 0
        End If
    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles btn_browse.Click
        fileDialog.ShowDialog()
    End Sub

    Private Sub fileDialog_FileOk(sender As Object, e As CancelEventArgs) Handles fileDialog.FileOk
        txt_file.Text = fileDialog.FileName
    End Sub

    Private Sub frmMain_Load(sender As Object, e As EventArgs) Handles Me.Load
        email.NewClient(clsSMTP.ServerType.Domain, "STRINGERSERVER", 0, "STRINGER", "office", "P4ssword", False)
    End Sub

    Public Sub getCompanies()
        'cycles through database and retrives all the ids of companies that are being sent the email
        Dim cmd As String = "Select CompanyID from Company where Newsletter=1"    'dummy statement
        Dim reader As SqlDataReader = sql.query(cmd)
        Try
            If reader.HasRows() Then
                Do While reader.Read()
                    If reader.GetInt32(0) <> Nothing Then
                        companies.Add(reader.GetInt32(0))
                    End If
                Loop
            Else
                MsgBox("Doesn't have rows")
            End If
        Catch ex As Exception
            MsgBox(ex.ToString)
            GoTo end_of_sub 'skips the rest of the function
        End Try
        'Closes reader and connection VERY IMPORTANT
end_of_sub:
        sql.closeConnection()
    End Sub

End Class
