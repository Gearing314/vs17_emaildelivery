﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class frmMain
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(frmMain))
        Me.btn_start = New System.Windows.Forms.Button()
        Me.timer = New System.Windows.Forms.Timer(Me.components)
        Me.txt_file = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.fileDialog = New System.Windows.Forms.OpenFileDialog()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.txt_interval = New System.Windows.Forms.TextBox()
        Me.btn_browse = New System.Windows.Forms.Button()
        Me.chk_HTML = New System.Windows.Forms.CheckBox()
        Me.pb_continous = New System.Windows.Forms.ProgressBar()
        Me.pb_overall = New System.Windows.Forms.ProgressBar()
        Me.lbl_current = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.currentTimer = New System.Windows.Forms.Timer(Me.components)
        Me.SuspendLayout()
        '
        'btn_start
        '
        Me.btn_start.Font = New System.Drawing.Font("Verdana Pro Cond", 27.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.btn_start.ForeColor = System.Drawing.Color.Red
        Me.btn_start.Location = New System.Drawing.Point(56, 163)
        Me.btn_start.Name = "btn_start"
        Me.btn_start.Size = New System.Drawing.Size(148, 55)
        Me.btn_start.TabIndex = 0
        Me.btn_start.Text = "START"
        Me.btn_start.UseVisualStyleBackColor = True
        '
        'timer
        '
        '
        'txt_file
        '
        Me.txt_file.Location = New System.Drawing.Point(15, 25)
        Me.txt_file.Name = "txt_file"
        Me.txt_file.Size = New System.Drawing.Size(234, 20)
        Me.txt_file.TabIndex = 1
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(12, 9)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(57, 13)
        Me.Label1.TabIndex = 2
        Me.Label1.Text = "Email File: "
        '
        'fileDialog
        '
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(12, 79)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(105, 13)
        Me.Label2.TabIndex = 3
        Me.Label2.Text = "Batch interval (mins):"
        '
        'txt_interval
        '
        Me.txt_interval.Location = New System.Drawing.Point(12, 95)
        Me.txt_interval.Name = "txt_interval"
        Me.txt_interval.Size = New System.Drawing.Size(234, 20)
        Me.txt_interval.TabIndex = 4
        '
        'btn_browse
        '
        Me.btn_browse.Location = New System.Drawing.Point(75, 47)
        Me.btn_browse.Name = "btn_browse"
        Me.btn_browse.Size = New System.Drawing.Size(117, 23)
        Me.btn_browse.TabIndex = 5
        Me.btn_browse.Text = "Browse"
        Me.btn_browse.UseVisualStyleBackColor = True
        '
        'chk_HTML
        '
        Me.chk_HTML.AutoSize = True
        Me.chk_HTML.Location = New System.Drawing.Point(15, 131)
        Me.chk_HTML.Name = "chk_HTML"
        Me.chk_HTML.Size = New System.Drawing.Size(94, 17)
        Me.chk_HTML.TabIndex = 7
        Me.chk_HTML.Text = "HTML format?"
        Me.chk_HTML.UseVisualStyleBackColor = True
        '
        'pb_continous
        '
        Me.pb_continous.Location = New System.Drawing.Point(15, 242)
        Me.pb_continous.MarqueeAnimationSpeed = 0
        Me.pb_continous.Name = "pb_continous"
        Me.pb_continous.Size = New System.Drawing.Size(231, 16)
        Me.pb_continous.Style = System.Windows.Forms.ProgressBarStyle.Marquee
        Me.pb_continous.TabIndex = 8
        '
        'pb_overall
        '
        Me.pb_overall.Location = New System.Drawing.Point(15, 279)
        Me.pb_overall.Name = "pb_overall"
        Me.pb_overall.Size = New System.Drawing.Size(231, 19)
        Me.pb_overall.TabIndex = 9
        '
        'lbl_current
        '
        Me.lbl_current.AutoSize = True
        Me.lbl_current.Location = New System.Drawing.Point(15, 223)
        Me.lbl_current.Name = "lbl_current"
        Me.lbl_current.Size = New System.Drawing.Size(0, 13)
        Me.lbl_current.TabIndex = 10
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(15, 265)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(87, 13)
        Me.Label4.TabIndex = 11
        Me.Label4.Text = "Overall Progress:"
        '
        'currentTimer
        '
        '
        'frmMain
        '
        Me.AcceptButton = Me.btn_start
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(262, 310)
        Me.Controls.Add(Me.Label4)
        Me.Controls.Add(Me.lbl_current)
        Me.Controls.Add(Me.pb_overall)
        Me.Controls.Add(Me.pb_continous)
        Me.Controls.Add(Me.chk_HTML)
        Me.Controls.Add(Me.btn_browse)
        Me.Controls.Add(Me.txt_interval)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.txt_file)
        Me.Controls.Add(Me.btn_start)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "frmMain"
        Me.Text = "Email Delivery"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btn_start As Button
    Friend WithEvents timer As Timer
    Friend WithEvents txt_file As TextBox
    Friend WithEvents Label1 As Label
    Friend WithEvents fileDialog As OpenFileDialog
    Friend WithEvents Label2 As Label
    Friend WithEvents txt_interval As TextBox
    Friend WithEvents btn_browse As Button
    Friend WithEvents chk_HTML As CheckBox
    Friend WithEvents pb_continous As ProgressBar
    Friend WithEvents pb_overall As ProgressBar
    Friend WithEvents lbl_current As Label
    Friend WithEvents Label4 As Label
    Friend WithEvents currentTimer As Timer
End Class
