﻿Imports System.Data.SqlClient
Public Class clsSqlTools
    Private myConn As SqlConnection
    Private myCmd As SqlCommand
    Private myReader As SqlDataReader

    Public Sub New(connectionString As String)
        myConn = New SqlConnection(connectionString)
    End Sub

    Public Function query(cmd As String) As SqlDataReader
        myCmd = myConn.CreateCommand
        'Code for executing cmd
        Try
            myCmd.CommandText = cmd
            myConn.Open()
            myReader = myCmd.ExecuteReader()
        Catch ex As Exception
            MsgBox(ex.ToString)
        End Try
        'getting data from query
        Return myReader
    End Function

    Public Sub closeConnection()
        Try
            myReader.Close()
            myConn.Close()
        Catch ex As Exception
            Debug.WriteLine(ex.ToString)
        End Try

    End Sub
End Class
