﻿Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
'Imports System.Threading
Imports System.ComponentModel

Public Class clsSMTP
#Region "Variables etc."
    Public Enum ServerType
        Domain
        Cloud
        Webmail
    End Enum

    Public Structure ReturnValue
        Public blnResult As Boolean
        Public strMessage As String
        Public strDetail As String
    End Structure

    Private _blnHandlerAdded As Boolean = False
    Private _blnMailSent As Boolean = False
    Private _clnt As SmtpClient
    Private _msg As MailMessage
#End Region

#Region "Events"
    Public Event SendCompleted(ByVal blnSuccess As Boolean, ByVal strMessage As String)
#End Region

#Region "Class handling"
    Public Function NewClient(ByVal enServerType As ServerType,
                         ByVal strServer As String,
                         ByVal intPort As Nullable(Of Int32),
                         ByVal strDomain As String,
                         ByVal strUsername As String,
                         ByVal strPassword As String,
                         ByVal blnSSL As Boolean) As ReturnValue
        Dim srvReturn As ReturnValue
        srvReturn.strMessage = ""
        srvReturn.strDetail = ""
        Try
            If enServerType = ServerType.Domain And strDomain.Length = 0 Then
                srvReturn.blnResult = False
                srvReturn.strMessage = "Domain must be specified for a domain server"
                Exit Try
            ElseIf enServerType = ServerType.Webmail And (Not intPort.HasValue OrElse intPort = 0) Then
                srvReturn.blnResult = False
                srvReturn.strMessage = "Port must be specified for a webmail server"
                Exit Try
            End If

            If intPort.HasValue AndAlso intPort > 0 Then
                _clnt = New SmtpClient(strServer, intPort)
            Else
                _clnt = New SmtpClient(strServer)
            End If
            _clnt.EnableSsl = blnSSL
            _clnt.UseDefaultCredentials = False

            If enServerType = ServerType.Domain Then
                _clnt.DeliveryMethod = SmtpDeliveryMethod.Network
                _clnt.Credentials = New NetworkCredential(strUsername, strPassword, strDomain)
            ElseIf enServerType = ServerType.Webmail Then
                _clnt.Credentials = New NetworkCredential(strUsername, strPassword)
            Else
                srvReturn.blnResult = False
                srvReturn.strMessage = "Server type not supported"
                Exit Try
            End If

            srvReturn.blnResult = True
        Catch ex As Exception
            ErrorMessage("NewClient()", ex)
            srvReturn.blnResult = False
            srvReturn.strMessage = ex.Message
            srvReturn.strDetail = If(ex.InnerException IsNot Nothing, ex.InnerException.Message, "")
        Finally
            If Not _blnHandlerAdded Then
                AddHandler _clnt.SendCompleted, AddressOf SendComplete
                _blnHandlerAdded = True
            End If
        End Try
        Return srvReturn
    End Function

    Public Function GetVersion() As System.Version
        Return System.Reflection.Assembly.GetExecutingAssembly().GetName().Version()
    End Function
#End Region

#Region "Mail handling"
    Public Function SendMessage(ByVal strFromAddress As String,
                                ByVal strToAddress As String,
                                ByVal strCCAddress As String,
                                ByVal strBCCAddress As String,
                                ByVal strSubject As String,
                                ByVal strMessage As String,
                                ByVal blnSynchronous As Boolean,
                                ByVal blnHTML As Boolean) As ReturnValue
        Dim srvReturn As ReturnValue
        Try
            Dim adrFrom As New MailAddress(strFromAddress)

            _msg = New MailMessage()
            _msg.From = adrFrom
            _msg.To.Add(strToAddress)
            If strCCAddress.Length > 0 Then
                _msg.CC.Add(strCCAddress)
            End If
            If strBCCAddress.Length > 0 Then
                _msg.Bcc.Add(strBCCAddress)
            End If
            _msg.Subject = strSubject
            _msg.Body = strMessage
            _msg.IsBodyHtml = blnHTML

            If blnSynchronous Then
                _clnt.Send(_msg)
                _msg.Dispose()
                srvReturn.strMessage = ""
                RaiseEvent SendCompleted(True, "[" & strSubject & "] - Message sent")
            Else
                _clnt.SendAsync(_msg, _msg.Subject)
                srvReturn.strMessage = "Sending to " & strToAddress & " ..."
            End If

            srvReturn.blnResult = True
            srvReturn.strDetail = ""
        Catch ex As Exception
            srvReturn.blnResult = False
            srvReturn.strMessage = ex.Message
            srvReturn.strDetail = If(ex.InnerException IsNot Nothing, ex.InnerException.Message, "")
        End Try
        Return srvReturn
    End Function

    Public Sub CancelSend()
        _clnt.SendAsyncCancel()
    End Sub

    Private Sub SendComplete(ByVal sender As Object, ByVal e As AsyncCompletedEventArgs)
        Try
            Dim strUserState As String = e.UserState.ToString

            If e.Cancelled Then
                RaiseEvent SendCompleted(False, "Send cancelled")
            ElseIf e.Error IsNot Nothing Then
                RaiseEvent SendCompleted(False, "Send error: " & e.UserState & ": " & e.Error.ToString)
            Else
                RaiseEvent SendCompleted(True, "[" & strUserState & "] - Message sent")
            End If
        Catch ex As Exception
            ErrorMessage("SendComplete()", ex)
        Finally
            _msg.Dispose()
        End Try
    End Sub

    'Private Sub AddAttachment(ByRef msg As MailMessage, ByVal strFile As String)
    '    Try
    '        Dim atcFile As Attachment = New Attachment(strFile, MediaTypeNames.Application.Pdf)
    '        Dim dpn As ContentDisposition = atcFile.ContentDisposition
    '        dpn.CreationDate = System.IO.File.GetCreationTime(strFile)
    '        dpn.ModificationDate = System.IO.File.GetLastWriteTime(strFile)
    '        dpn.ReadDate = System.IO.File.GetLastAccessTime(strFile)

    '        msg.Attachments.Add(atcFile)
    '    Catch ex As Exception
    '        MsgBox("AddAttachment error: " & ex.Message)
    '    End Try
    'End Sub
#End Region

#Region "Error handling"
    Private Sub ErrorMessage(ByRef strMethod As String, ByRef ex As System.Exception)
        MsgBox("clsLibSMTP." & strMethod & " error:" & vbCrLf & vbCrLf &
               ex.Message & vbCrLf & vbCrLf &
               ex.InnerException.Message,
               MsgBoxStyle.Critical + MsgBoxStyle.OkOnly)
    End Sub
#End Region
End Class
